#ifndef _COMMON_H
#define _COMMON_H

static const u_int32_t BUF_SIZE = 4096;

template <typename T>
inline T maximum(T x, T y)
{
  return ((x > y) ? x : y);
}

template <typename T>
inline T minimum(T x, T y)
{
  return ((x > y) ? y : x);
}

enum auth_db_action {ADD_USER, DELETE_USER, CHANGE_PASSWORD};

static const int ACTION_CODE_OFFSET = 0;
static const int USERNAME_LEN_OFFSET = ACTION_CODE_OFFSET + 1;
// static const int USERNAME_BEGIN_OFFSET = ;
static const int PASSWORD_LEN_OFFSET = USERNAME_LEN_OFFSET + 1;
static const int DATA_BEGIN_OFFSET = PASSWORD_LEN_OFFSET + 1;

// TODO: It would be nicer to set these in the build system, rather than here. (NOTE: scgi_gateway.py also uses WEB_ROOT_DIR; currently it's included in the source there too. Don't know a good way to handle this issue.)
#define LIBEXEC_DIR "/usr/local/lib/project/libexec"
#define WEB_ROOT_DIR "/var/project/document_root"
#define UM_COMM_DIR_SUFFIX "/um_comm"
#define SCGI_COMM_DIR_SUFFIX "/scgi_comm"
#define SCGI_USER_DIR_SUFFIX "_sockdir"
#define SCGI_SOCKET_SUFFIX "/socket"
#define HOME_DIRS_DIR_SUFFIX "/home"
#define SKEL_DIR "/usr/local/share/project/skel"

#endif
