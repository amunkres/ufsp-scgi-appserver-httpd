#!/usr/bin/python
# -*- coding: utf-8 -*-
import colubrid
import subprocess
import urllib

PIECE_BEGIN = \
"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
"""

FORM = \
"""<head><title>Create new user</title></head>
<body>
<form action="%s" method="post">
Username: <input name="username" type="text" /><br />
Password: <input name="password" type="password" /><br />
<input value="Send" type="submit" />
</form>
</body>
"""

FAILURE = \
"""<head><title>Error creating user</title></head>
<body>
<p>Could not create user "%s": %s</p>
</body>
"""

SUCCESS = \
"""<head><title>Created user</title></head>
<body>
<p>Created user "%s".</p>
</body>
"""

PIECE_END = \
"""</html>
"""

def applySubst(piece, params):
  return piece % tuple(map((lambda p: p.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")), params))

class UserReg(colubrid.BaseApplication):
  def __init__(self, environ, start_response):
    # NOTE: I don't know if it matters, but I'm setting our custom attributes before calling parent class's constructor.
    self.uratt_user = environ.get("REMOTE_USER", "REMOTE_USER_was_not_set")
    # This part comes from ( http://www.python.org/dev/peps/pep-0333/#url-reconstruction ):
    self.uratt_url_without_query = environ['wsgi.url_scheme']+'://'
    if environ.get('HTTP_HOST'):
      self.uratt_url_without_query += environ['HTTP_HOST']
    else:
      self.uratt_url_without_query += environ['SERVER_NAME']
      if environ['wsgi.url_scheme'] == 'https':
        if environ['SERVER_PORT'] != '443':
          self.uratt_url_without_query += ':' + environ['SERVER_PORT']
      else:
        if environ['SERVER_PORT'] != '80':
          self.uratt_url_without_query += ':' + environ['SERVER_PORT']
    self.uratt_url_without_query += urllib.quote(environ.get('SCRIPT_NAME',''))
    self.uratt_url_without_query += urllib.quote(environ.get('PATH_INFO',''))
    super(UserReg, self).__init__(environ, start_response)
  
  def process_request(self):
    try:
      username = self.request.values["username"]
      password = self.request.values["password"]
    except KeyError:
      return colubrid.HttpResponse(applySubst(PIECE_BEGIN + FORM + PIECE_END, [self.uratt_url_without_query]), [('Content-Type', 'text/html')], 200)
    sub = subprocess.Popen(["/var/project/document_root/sbin/createuser", username, password], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (ignored, error_text) = sub.communicate()
    exit_status = sub.wait()
    if 42 == exit_status:
      return colubrid.HttpResponse(applySubst(PIECE_BEGIN + FAILURE + PIECE_END, [username, error_text]), [('Content-Type', 'text/html')], 200)
    return colubrid.HttpResponse(applySubst(PIECE_BEGIN + SUCCESS + PIECE_END, [username]), [('Content-Type', 'text/html')], 200)

projApp = UserReg
