#!/usr/bin/python
# -*- coding: utf-8 -*-

import flup.server.scgi
import urlparse
import urllib
import os
import stat
import pwd
import imp
import sys

WEB_ROOT_DIR = os.path.realpath("/var/project/document_root")

STATUS_400 = "400 Bad Request"
STATUS_403 = "403 Forbidden"
STATUS_404 = "404 Not Found"
HTML_PART1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html><head><title>"
HTML_PART2 = "</title></head><body><p>"
HTML_PART3 = "</p></body>\n"

def fullSplitPath(path_str):
  head = path_str
  prev_head = None
  retval = []
  while prev_head != head:
    prev_head = head
    (head, part) = os.path.split(prev_head)
    retval.insert(0, part)
  del retval[0]
  return (retval, head)

def escapeXmlChars(string):
  return string.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

def requestForwarder(environ, start_response):
  split_url = urlparse.urlparse(environ["REQUEST_URI"])
  # Set the QUERY_STRING environment variable to what it should be.
  rel_path = urllib.unquote(split_url.path)
  if "/" != rel_path[0]:
    start_response(STATUS_400, [("Content-Type", "text/html")])
    return ["%s%s%sREQUEST_URI path does not begin with \"/\".%s" % (HTML_PART1, STATUS_400, HTML_PART2, HTML_PART3)]
  environ["QUERY_STRING"] = split_url.query
  # Traverse the full_path, treating it as a path relative to WEB_ROOT_DIR.
  # WARNING: Do NOT allow it to use ".." to escape from WEB_ROOT_DIR!
  # We want to find the first part of full_path which refers to a file, rather than a directory. The path up and including that file will become SCRIPT_NAME; the remaining part of the path will become PATH_INFO.
  # As a special case, if the first part is "~name", go to the home directory of user "name"
  
  (path_parts, system_root) = fullSplitPath(rel_path)
  try:
    first_part = path_parts[0]
  except IndexError, e:
    # In this case, rel_path is probably "/".
    first_part = None
  if first_part and "~" == first_part[0]:
    # Prepend the specified user's home dir to rel_path, giving the full_path.
    dir_user = first_part[1:]
    try:
      pw_entry = pwd.getpwnam(dir_user)
    except KeyError, e:
      start_response(STATUS_404, [("Content-Type", "text/html")])
      return ["%s%s%sUser \"%s\" does not exist.%s" % (HTML_PART1, STATUS_404, HTML_PART2, escapeXmlChars(dir_user), HTML_PART3)]
    home_path = os.path.realpath(pw_entry.pw_dir)
    # Actually do the prepending here.
    full_path = os.path.realpath(os.path.join(*([home_path] + path_parts[1:])))
  else:
    # In the case no user's home dir was specified, just prepend WEB_ROOT_DIR to rel_path, giving the full_path.
    full_path = os.path.realpath(os.path.join(*([WEB_ROOT_DIR] + path_parts)))
    sys.stderr.write("DEBUG: WEB_ROOT_DIR is \"%s\", path_parts is %s, full_path is \"%s\"\n" % (str(WEB_ROOT_DIR), str(path_parts), str(full_path)))
  
  # Both full_path and WEB_ROOT_DIR were produced by os.path.realpath, which removes all ".."s, "."s, superfluous "/"s, and symlinks. So, if WEB_ROOT_DIR is a prefix of full_path, then we can be sure that the file/directory it points to is within the dir.
  if full_path[:len(WEB_ROOT_DIR)] != WEB_ROOT_DIR:
    start_response(STATUS_403, [("Content-Type", "text/html")])
    return ["%s%s%sYou are not allowed to specify a pathname which is outside of the server root directory!%s" % (HTML_PART1, STATUS_403, HTML_PART2, HTML_PART3)]
  
  (path_parts, system_root) = fullSplitPath(full_path)
  script_name = system_root
  found_non_dir = False
  # NOTE: Checking begins at the first pathname element *after* system_root. This is ok, because system_root is *always* a directory.
  for part in path_parts:
    script_name = os.path.join(script_name, part)
    #sys.stderr.write("DEBUG: calling stat on path \"%s\"\n" % script_name)
    try:
      st = os.stat(script_name)
    except OSError, fs_error:
      if 2 == fs_error.errno: # errno 2 is "No such file or directory"
        status = STATUS_404
      else:
        status = STATUS_403
      start_response(status, [("Content-Type", "text/html")])
      #return ["%s%s%s%s%s" % (HTML_PART1, status, HTML_PART2, str(fs_error), HTML_PART3)]
      return ["%s%s%sError accessing pathname \"%s\": %s%s" % (HTML_PART1, status, HTML_PART2, escapeXmlChars(rel_path), escapeXmlChars(os.strerror(fs_error.errno)), HTML_PART3)]
    if not stat.S_ISDIR(st.st_mode):
      found_non_dir = True
      break
  
  if not found_non_dir:
    # All elements of "full_path" are directories, not files. This is an error, because we require the module to be a single-file module. (This is because we want to be able to detect what part of "full_path" is the PATH_INFO part.)
    start_response(STATUS_403, [("Content-Type", "text/html")])
    return ["%s%s%sPathname \"%s\" refers to a directory, not a single-file Python module.%s" % (HTML_PART1, STATUS_403, HTML_PART2, escapeXmlChars(rel_path), HTML_PART3)]
  
  (module_dir, module_filename) = os.path.split(script_name)
  # Import the Python module module_file which is located in the directory module_dir. This module is required to be a single-file module (of course, it can import other modules)
  
  # Determine the module name from the module filename. (Currenntly, we support only single-file modules, not packages; this makes it easies to determine the PATH_INFO value.)
  module_name = None
  for (suffix, mode, typ) in imp.get_suffixes():
    if suffix == module_filename[-len(suffix):]:
      module_name = module_filename[:-len(suffix)]
  if None == module_name:
    start_response(STATUS_403, [("Content-Type", "text/html")])
    return ["%s%s%sFile \"%s\" does not have the appropriate extension for a Python module file.%s" % (HTML_PART1, STATUS_403, HTML_PART2, escapeXmlChars(module_filename), HTML_PART3)]
  
  # Import the module unless it has already been imported. (Currently we don't have a way of forcing a reload except for killing the SCGI server; the web server should then restart the SCGI server.)
  imp.acquire_lock()
  try:
    try:
      module = sys.modules[module_name]
    except KeyError:
      (module_file, module_path, module_desc) = imp.find_module(module_name, [module_dir])
      module = imp.load_module(module_name, module_file, module_path, module_desc)
  except ImportError, msg:
    imp.release_lock()
    start_response(STATUS_403, [("Content-Type", "text/html")])
    return ["%s%s%sCould not load module file \"%s\": %s%s" % (HTML_PART1, STATUS_403, HTML_PART2, escapeXmlChars(module_filename), escapeXmlChars(repr(msg)), HTML_PART3)]
  imp.release_lock()
  
  environ["SCRIPT_NAME"] = script_name[len(WEB_ROOT_DIR):]
  environ["PATH_INFO"] = full_path[len(script_name):]
  environ["REMOTE_USER"] = pwd.getpwuid(os.getuid()).pw_name
  
  try:
    app_to_run = module.projApp
  except:
    start_response(STATUS_403, [("Content-Type", "text/html")])
    return ["%s%s%sModule file \"%s\" is not an application: %s%s" % (HTML_PART1, STATUS_403, HTML_PART2, escapeXmlChars(module_filename), escapeXmlChars(repr(sys.exc_info()[1])), HTML_PART3)]
  
  return app_to_run(environ, start_response)

if "__main__" == __name__:
  # print sys.argv
  flup.server.scgi.WSGIServer(application=requestForwarder, bindAddress=sys.argv[1], umask=0).run()
