#!/usr/bin/python
# -*- coding: utf-8 -*-
import colubrid
import urllib
import pwd
import os
import sys
import stat
import subprocess

# NOTE: For a real-world application, it may be better to use a templating system (for example, http://jinja.pocoo.org/2/ ), but here I'm just going to use simple XML-escaping and string-substitution.

PIECE_BEGIN = \
"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head><title>Viewing directory %s</title></head>
<body>
<h2>Viewing directory %s</h2>
<p>
"""

PIECE_DIR_ENTRY = \
"""<a href="%s">%s</a><br />
"""

PIECE_END = \
"""</p>
<hr />
<form action="%s" method="post" enctype="multipart/form-data">
Upload a file: <input name="uploaded" type="file" />
<input name="path" type="hidden" value="%s" />
<input value="Send" type="submit" />
</form>
</body>
</html>
"""

NO_USER_ERROR = \
"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head><title>Error</title></head>
<body>
<h2>Error</h2>
<p>No user named "%s" exists.</p>
</body>
</html>
"""

GENERIC_ERROR = \
"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head><title>Error</title></head>
<body>
<h2>Error</h2>
<p>%s</p>
</body>
</html>
"""

def applySubst(piece, params):
  return piece % tuple(map((lambda p: p.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")), params))
  

class DirViewer(colubrid.BaseApplication):
  def __init__(self, environ, start_response):
    # NOTE: I don't know if it matters, but I'm setting our custom attributes before calling parent class's constructor.
    self.dvatt_user = environ.get("REMOTE_USER", "REMOTE_USER_was_not_set")
    # This part comes from ( http://www.python.org/dev/peps/pep-0333/#url-reconstruction ):
    self.dvatt_url_without_query = environ['wsgi.url_scheme']+'://'
    if environ.get('HTTP_HOST'):
      self.dvatt_url_without_query += environ['HTTP_HOST']
    else:
      self.dvatt_url_without_query += environ['SERVER_NAME']
      if environ['wsgi.url_scheme'] == 'https':
        if environ['SERVER_PORT'] != '443':
          self.dvatt_url_without_query += ':' + environ['SERVER_PORT']
      else:
        if environ['SERVER_PORT'] != '80':
          self.dvatt_url_without_query += ':' + environ['SERVER_PORT']
    self.dvatt_url_without_query += urllib.quote(environ.get('SCRIPT_NAME',''))
    self.dvatt_url_without_query += urllib.quote(environ.get('PATH_INFO',''))
    super(DirViewer, self).__init__(environ, start_response)
  
  def process_request(self):
    try:
      home_dir = pwd.getpwnam(self.dvatt_user).pw_dir
    except:
      return colubrid.HttpResponse(applySubst(NO_USER_ERROR, [self.dvatt_user]), [('Content-Type', 'text/html')], 404)
    try:
      # The target_path will be the "path" parameter resolved relative to home_dir (or just "path" if "path" is absolute).
      target_path = os.path.join(home_dir, self.request.values["path"])
    except KeyError:
      # If no "path" parameter, we view the current directory.
      target_path = home_dir
    # Call os.path.realpath so that we don't need special case for symlinks.
    target_path = os.path.realpath(target_path)
    try:
      # Check if target_path is a file or is a directory.
      st = os.stat(target_path)
      if stat.S_ISDIR(st.st_mode):
        # Check if the client is trying to upload files.
        if 0 != len(self.request.files):
          # The client is trying to upload files.
          for sent_file in self.request.files.values():
            # Write the file here. (Colubrid doesn't seem to have an efficient API for sending large binary file uploads, grrrr.)
            # WARNING! Does Colubrid sanitize filenames of uploaded files? I'll call os.path.split on the filename just to make sure.
            outfile = open(os.path.join(target_path, os.path.split(sent_file.filename)[1]), "wb")
            # outfile.write(sent_file.read()) # For now, use data atribute instead
            outfile.write(sent_file.data)
          # After uploads have finished, we will continue and view the directory.
        # Whether or not any files were submitted, we view the directory.
        # TODO: implement directory listing
        def makeDirListGenerator(entries):
          out_block = applySubst(PIECE_BEGIN, [target_path, target_path])
          for entry in entries:
            out_block += applySubst(PIECE_DIR_ENTRY, [self.dvatt_url_without_query + "?" + urllib.urlencode([("path", os.path.join(target_path, entry))]), entry])
            if 4096 <= len(out_block):
              yield out_block
              out_block = ""
          out_block += applySubst(PIECE_END, [self.dvatt_url_without_query, target_path])
          yield out_block
          return
        return colubrid.HttpResponse(makeDirListGenerator([".", ".."] + os.listdir(target_path)), [('Content-Type', 'text/html')], 200)
      else:
        # In this case, target_path isn't a directory.
        # If it's a regular file, we try to use /usr/bin/file to get its MIME type; otherwise, default to application/octet-stream.
        headers = []
        if stat.S_ISREG(st.st_mode):
          sub = subprocess.Popen(["/usr/bin/file", "--brief", "--mime", target_path], shell=False, stdout=subprocess.PIPE)
          (out_text, ignored) = sub.communicate()
          sub.wait()
          mime_info = out_text.strip()
          headers.append(("Content-Type", mime_info))
          headers.append(("Content-Length", str(st.st_size)))
        else:
          mime_info = "application/octet-stream"
        infile = open(target_path, "rb")
        # Make an iterable that returns 4k blocks of the file, use it as data source for HttpResponse.
        def makeDataGenerator(infile):
          while True:
            block_read = infile.read(4096)
            if "" != block_read:
              yield block_read
            else:
              return
        return colubrid.HttpResponse(makeDataGenerator(infile), headers, 200)
    except:
      return colubrid.HttpResponse(applySubst(GENERIC_ERROR, [str(sys.exc_info()[1])]), [('Content-Type', 'text/html')], 403)

projApp = DirViewer
