#include <string>
#include <limits>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

#include "common.h"

using namespace std;

static sockaddr_un sun_sizecheck;

int main(int argc, char **argv)
{
  if (3 > argc)
  {
    fprintf(stderr, "Usage: createuser USERNAME PASSWORD\n");
    exit(42);
  }
  
  // Apparently, we need to fully become root in order to use useradd non-interactively.
  passwd *super_pw_entry(getpwuid(0));
  if (NULL == super_pw_entry)
  {
    fprintf(stderr, "Error: the superuser does not exist in system password database!?\n");
    exit(42);
  }
  if (-1 == initgroups(super_pw_entry->pw_name, super_pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: while becoming superuser, initgroups(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setgid(super_pw_entry->pw_gid))
  {
    fprintf(stderr, "Error: while becoming superuser, setgid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  if (-1 == setuid(super_pw_entry->pw_uid))
  {
    fprintf(stderr, "Error: while becoming superuser, setuid(3) failed (%s)\n", strerror(errno));
    exit(42);
  }
  
  // Check that we won't exceed the maximum message size before we do anything else.
  size_t username_len(strlen(argv[1])), password_len(strlen(argv[2]));
  if (size_t(BUF_SIZE) < (username_len + (DATA_BEGIN_OFFSET * sizeof(u_int32_t))))
  {
    fprintf(stderr, "Error: username is too long\n");
    exit(42);
  }
  if (size_t(BUF_SIZE) < (password_len + (DATA_BEGIN_OFFSET * sizeof(u_int32_t))))
  {
    fprintf(stderr, "Error: password is too long\n");
    exit(42);
  }
  const size_t msg_len((DATA_BEGIN_OFFSET * sizeof(u_int32_t)) + username_len + password_len);
  if (size_t(BUF_SIZE) < msg_len)
  {
    fprintf(stderr, "Error: username and password together are too long\n");
    exit(42);
  }
  
  // Create the "scgi_comm" dir for user with mode 0750
  string scgi_dir(WEB_ROOT_DIR SCGI_COMM_DIR_SUFFIX "/");
  scgi_dir.append(argv[1]);
  scgi_dir.append(SCGI_USER_DIR_SUFFIX);
  string scgi_socket_path(scgi_dir);
  scgi_socket_path.append(SCGI_SOCKET_SUFFIX);
  if (sizeof(sun_sizecheck.sun_path) < scgi_socket_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", scgi_socket_path.c_str(), sizeof(sun_sizecheck.sun_path));
    exit(42);
  }
  mode_t old_umask(umask(0));
  int retval(mkdir(scgi_dir.c_str(), 0750));
  if (-1 == retval)
  {
    fprintf(stderr, "Error: Could not create SCGI socket directory \"%s\" (%s)\n", scgi_dir.c_str(), strerror(errno));
    exit(42);
  }
  umask(old_umask);
  
  // Create socket for sending user creation message to server
  int um_fd(socket(AF_LOCAL, SOCK_DGRAM, 0));
  int flags(fcntl(um_fd, F_GETFD));
  flags |= FD_CLOEXEC;
  fcntl(um_fd, F_SETFD, flags);
  sockaddr_un um_addr;
  um_addr.sun_family = AF_UNIX;
  string um_path(WEB_ROOT_DIR UM_COMM_DIR_SUFFIX "/um_socket");
  if (sizeof(sun_sizecheck.sun_path) < um_path.size())
  {
    fprintf(stderr, "Error: Unix socket pathname \"%s\" exceeds maximum length of %d\n", um_path.c_str(), sizeof(sun_sizecheck.sun_path));
    exit(42);
  }
  strncpy(um_addr.sun_path, um_path.c_str(), sizeof(sun_sizecheck.sun_path));
  retval = connect(um_fd, static_cast<sockaddr *>(static_cast<void *>(&um_addr)), sizeof(um_addr));
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not connect to pathname \"%s\" (%s)\n", um_path.c_str(), strerror(errno));
    exit(42);
  }
  
  // Use "useradd" to create a user.
  pid_t pid(fork());
  if (0 == pid)
  {
    static char *const envp[] = {static_cast<char *>(NULL)};
    execle("/usr/sbin/useradd", "/usr/sbin/useradd", "--base-dir", WEB_ROOT_DIR HOME_DIRS_DIR_SUFFIX, "--create-home", "--skel", SKEL_DIR, "--gid", "projusers", "--no-user-group", "--shell", "/bin/false", argv[1], static_cast<char *>(NULL), envp);
    fprintf(stderr, "Error: failed to exec /usr/sbin/useradd (%s)\n", strerror(errno));
    exit(42);
  }
  int status;
  retval = wait(&status);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: wait(2) for useradd process failed (%s)\n", strerror(errno));
  }
  if (WIFEXITED(status))
  {
    retval = WEXITSTATUS(status);
    switch (retval)
    {
      case 42:
        fprintf(stderr, "Error: subprocess failed to exec /usr/sbin/useradd\n");
        rmdir(scgi_dir.c_str());
        exit(42);
        break;
        
      case 0:
        // It succeeded. Only in this case do we continue.
        break;
        
      default:
        fprintf(stderr, "Error: /usr/sbin/useradd subprocess error-exited with status %d\n", retval);
        rmdir(scgi_dir.c_str());
        exit(42);
        break;
    }
  }
  
  // Change the ownership of the  SCGI socket dir to user <the new user>, group "projhttpd".
  passwd *pw_entry(getpwnam(argv[1]));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: user \"%s\" does not exist in system password database (strange, since we created it just now!)\n", argv[1]);
    rmdir(scgi_dir.c_str());
    exit(42);
  }
  group *gr_entry(getgrnam("projhttpd"));
  if (NULL == pw_entry)
  {
    fprintf(stderr, "Error: group \"projhttpd\" does not exist in system password database\n");
    rmdir(scgi_dir.c_str());
    exit(42);
  }
  retval = chown(scgi_dir.c_str(), pw_entry->pw_uid, gr_entry->gr_gid);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: could not chown \"%s\" to user \"%s\" and group \"projhttpd\" (%s)\n", scgi_dir.c_str(), argv[1], strerror(errno));
    rmdir(scgi_dir.c_str());
    exit(42);
  }
  
  // Prepare the message to send to the server.
  char temp_buf[msg_len];
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + ACTION_CODE_OFFSET) = u_int32_t(ADD_USER);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + USERNAME_LEN_OFFSET) = u_int32_t(username_len);
  *(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + PASSWORD_LEN_OFFSET) = u_int32_t(password_len);
  memcpy(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET), argv[1], username_len);
  memcpy(static_cast<void *>(static_cast<char *>(static_cast<void *>(static_cast<u_int32_t *>(static_cast<void *>(temp_buf)) + DATA_BEGIN_OFFSET)) + username_len), argv[2], password_len);
  
  // Send it.
  retval = send(um_fd, static_cast<void *>(temp_buf), msg_len, 0);
  if (-1 == retval)
  {
    fprintf(stderr, "Error: send(2) to send message to server failed (%s)\n", strerror(errno));
    rmdir(scgi_dir.c_str());
    exit(42);
  }
  
  // We're done.
  exit(0);
}
